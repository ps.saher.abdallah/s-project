#!/bin/bash


SHAvalue=${CI_COMMIT_SHA}

COMMIT_TIME=$(git show -s --format=%ct $CI_COMMIT_SHA)

COMMIT_DATE=$(date -d @$COMMIT_TIME +"%y%m%d")

NEW_VERSION=${COMMIT_DATE}-${CI_COMMIT_SHA:0:8}

echo "$NEW_VERSION"
