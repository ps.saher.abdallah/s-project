FROM openjdk:11

RUN useradd saher

WORKDIR /app
COPY /target/assignment-0.0.1-SNAPSHOT.jar .

USER saher

ENV SPRING_PROFILES_ACTIVE=h2
ENV JAVA_OPTS=""

CMD ["java", "-jar", "assignment-0.0.1-SNAPSHOT.jar"]
