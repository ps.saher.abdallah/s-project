#!/bin/bash

dbPodName="mysql-deployment-0"
maxRetries=5

for ((i=1; i<=$maxRetries; i++)); do
    podStatus=$(kubectl get pods $dbPodName -o jsonpath='{.status.phase}')

    if [ "$podStatus" -eq 'Running' ]; then
        echo "Mysql is running"
        exit 0  
    else
        echo "Attempt $i: Database status is $podStatus"
    fi

done

echo "Failed to connect to the database after $maxRetries retries. Exiting."
exit 1  
